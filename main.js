$(document).ready(function(){
   $("a.top-menu-link").click(function () {
       $("html, body").animate({
       scrollTop: $($(this).attr("href")).offset().top -50 + "px"}, 1000);
      return false;
   });
    $(window).scroll(function(){
        if($(this).scrollTop() > screen.height){
            $('.back-to-top').fadeIn();
        }else{
            $('.back-to-top').fadeOut();
        }
    });

  $('.back-to-top').click(function (event){
      $("html, body").animate({scrollTop: 0}, 1000);
      return false;
  });

  $(".slide-toggle").click(function (){
      $(".Latest-posts").slideToggle();

      if($(".slide-toggle").text() === "Скрыть"){
          $(".slide-toggle").text("Отобразить");
      }else{
          $(".slide-toggle").text("Скрыть");
      }
  });
});
